package org.desz.model.validation;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.commons.lang3.EnumUtils;
import org.desz.inttoword.language.ProvLang;
import org.desz.model.IntToWrdReq;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestProvLangValidator {

	static Validator validator;
	@BeforeClass
	public static void setUp() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	@Test
	public void testValidProvLang() {

		IntToWrdReq intToWrdReq = new IntToWrdReq();
		intToWrdReq.setNumber("xx");
		// bn.setValidEnum(ProvLang.DE.getCode());
		ProvLang provLang = EnumUtils.getEnum(ProvLang.class, "EMPTY");
		assertNotNull(provLang);
		intToWrdReq.setProvisionedLanguage(provLang);
		Set<ConstraintViolation<IntToWrdReq>> set = validator
				.validate(intToWrdReq);
		assertTrue("Expect TWO(2) violations", set.size() == 2);
	}

	@Test
	public void testInValidProvLangNullInt() {

		IntToWrdReq intToWrdReq = new IntToWrdReq();
		intToWrdReq.setProvisionedLanguage(ProvLang.EMPTY);
		Set<ConstraintViolation<IntToWrdReq>> set = validator
				.validate(intToWrdReq);
		assertTrue("Expect ONE (1) violation", set.size() == 1);
	}

}
