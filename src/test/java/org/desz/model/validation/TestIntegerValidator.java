package org.desz.model.validation;

import static org.junit.Assert.assertTrue;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.desz.inttoword.language.ProvLang;
import org.desz.model.IntToWrdReq;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestIntegerValidator {

	static Validator validator;
	private IntToWrdReq intToWrdReq;

	@BeforeClass
	public static void setUp() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	@Before
	public void init() {
		intToWrdReq = new IntToWrdReq();
	}

	@Test
	public void testNonNumeric() {

		intToWrdReq.setNumber("xx");
		intToWrdReq.setProvisionedLanguage(ProvLang.UK);
		Set<ConstraintViolation<IntToWrdReq>> set = validator
				.validate(intToWrdReq);
		assertTrue("Expect ONE(1) violation", set.size() == 1);
	}

	@Test
	public void testCharAndNumeric() {

		intToWrdReq.setNumber("22xx1");
		intToWrdReq.setProvisionedLanguage(ProvLang.UK);
		Set<ConstraintViolation<IntToWrdReq>> set = validator
				.validate(intToWrdReq);
		assertTrue("Expect ONE(1) violation", set.size() == 1);
	}

	@Test
	public void testNonNumericProvLangEmpty() {

		intToWrdReq.setNumber("xx");
		intToWrdReq.setProvisionedLanguage(ProvLang.EMPTY);
		Set<ConstraintViolation<IntToWrdReq>> set = validator
				.validate(intToWrdReq);
		assertTrue("Expect TWO(2) violations", set.size() == 2);
	}

	@Test
	public void testRequestValid() {

		intToWrdReq.setNumber("10");
		intToWrdReq.setProvisionedLanguage(ProvLang.UK);
		Set<ConstraintViolation<IntToWrdReq>> set = validator
				.validate(intToWrdReq);
		assertTrue("expect ZERO(0) violation", set.size() == 0);
	}
	@Test
	public void testIntMax() {

		intToWrdReq.setNumber("2147483647");
		intToWrdReq.setProvisionedLanguage(ProvLang.UK);
		Set<ConstraintViolation<IntToWrdReq>> set = validator
				.validate(intToWrdReq);
		assertTrue("expect ZERO(0) violation", set.size() == 0);
	}

	@Test
	public void testIntMaxExceeded() {

		intToWrdReq.setNumber("2147483648");
		intToWrdReq.setProvisionedLanguage(ProvLang.UK);
		Set<ConstraintViolation<IntToWrdReq>> set = validator
				.validate(intToWrdReq);
		assertTrue("expect ONE(1) violation", set.size() == 1);
	}

	@Test
	public void testLessThanZero() {

		IntToWrdReq intToWrdReq = new IntToWrdReq();
		intToWrdReq.setNumber("-1");
		intToWrdReq.setProvisionedLanguage(ProvLang.UK);
		Set<ConstraintViolation<IntToWrdReq>> set = validator
				.validate(intToWrdReq);
		assertTrue("expect ONE(1) violation", set.size() == 1);
	}

}
