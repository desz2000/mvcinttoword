/**
 * 
 */
package org.desz.mvc.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;
//import org.apache.log4j.Logger;
import java.util.logging.Logger;

import org.desz.inttoword.language.ProvLangFactoryParts.UkError;
import org.desz.model.IntToWrdResp;
import org.desz.mvc.controller.config.IntToWordControllerConfig;
import org.springframework.test.context.ActiveProfiles;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpInputMessage;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author des Test responses of IntToWordController to JSON payloads
 * 
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = IntToWordControllerConfig.class)
@WebAppConfiguration
@ActiveProfiles({"dev", "cloud"})
public class TestIntToWordRestful {

	protected final Logger log = Logger
			.getLogger(TestIntToWordRestful.class.getName());

	// JSON payloads
	private String jsonValidNumberAndProv = "{\"provisionedLanguage\":\"UK\",\"number\":\"1000000\"}";

	private String jsonMissingNumber = "{\"provisionedLanguage\":\"UK\"}";

	private String jsonNullNumber = "{\"provisionedLanguage\":\"UK\",\"number\": null}";

	private String jsonEmptyProvNullNumber = "{\"provisionedLanguage\":\"EMPTY\",\"number\": null}";

	private String jsonUkNegativeNumber = "{\"provisionedLanguage\":\"UK\",\"number\":\"-100\"}";

	private String jsonEmptyProvLangValidInt = "{\"provisionedLanguage\":\"EMPTY\",\"number\":\"900\"}";

	private MappingJackson2HttpMessageConverter jack2HttpMsgCvtr;

	@Autowired
	private MockHttpServletRequest mockHttpRequest;
	@Autowired
	private MockHttpServletResponse mockHttpResponse;

	private static final String STATUS_200_MSG = "Http status should be 200";

	@Autowired
	private RequestMappingHandlerAdapter handlerAdapter;

	@Autowired
	private RequestMappingHandlerMapping requestHandlerMapping;

	private Object handler;

	@Before
	public void setUp() throws Exception {

		ObjectMapper objectMapper = new ObjectMapper();
		jack2HttpMsgCvtr = new MappingJackson2HttpMessageConverter();
		jack2HttpMsgCvtr.setObjectMapper(objectMapper);
		mockHttpRequest.setMethod("POST");
		mockHttpRequest.setContentType(MediaType.APPLICATION_JSON_VALUE);
		mockHttpRequest.setRequestURI(IntToWordController.POSTREQ);
		handler = requestHandlerMapping.getHandler(mockHttpRequest)
				.getHandler();
	}

	@Test
	public void testProvLangEmptyAndNullNumber() throws Exception {

		mockHttpRequest.setContent(jsonEmptyProvNullNumber.getBytes());
		handlerAdapter.handle(mockHttpRequest, mockHttpResponse, handler);

		assertEquals(STATUS_200_MSG, 200, mockHttpResponse.getStatus());

		String jsonResult = mockHttpResponse.getContentAsString();
		log.info(String.format("IntToWordController Response, %s", jsonResult));
		MockHttpInputMessage httpReq = new MockHttpInputMessage(
				jsonResult.getBytes("UTF-8"));
		IntToWrdResp result = (IntToWrdResp) jack2HttpMsgCvtr
				.read(IntToWrdResp.class, httpReq);
		assertEquals("Expected 1 (ONE) validation failures", 1,
				result.getErrorList().size());

	}

	@Test
	public void testProvLangEmpty() throws Exception {

		mockHttpRequest.setContent(jsonEmptyProvLangValidInt.getBytes());
		handlerAdapter.handle(mockHttpRequest, mockHttpResponse, handler);

		assertEquals(STATUS_200_MSG, 200, mockHttpResponse.getStatus());

		final String jsonResult = mockHttpResponse.getContentAsString();
		assertNotNull("json response should not be null", jsonResult);
		MockHttpInputMessage inputMessage = new MockHttpInputMessage(
				jsonResult.getBytes("UTF-8"));
		IntToWrdResp int2WrdResp = (IntToWrdResp) jack2HttpMsgCvtr
				.read(IntToWrdResp.class, inputMessage);
		assertEquals("Expected 1 (one) validation failure.", 1,
				int2WrdResp.getErrorList().size());

	}

	@Test
	public void testForInvalidMin() throws Exception {

		mockHttpRequest.setContent(jsonUkNegativeNumber.getBytes());
		handlerAdapter.handle(mockHttpRequest, mockHttpResponse, handler);
		assertEquals(STATUS_200_MSG, 200, mockHttpResponse.getStatus());

		String jsonResult = mockHttpResponse.getContentAsString();
		log.info("mockHttpResponse from IntToWordController: " + jsonResult);
		MockHttpInputMessage inputMessage = new MockHttpInputMessage(
				jsonResult.getBytes("UTF-8"));
		IntToWrdResp int2WrdResp = (IntToWrdResp) jack2HttpMsgCvtr
				.read(IntToWrdResp.class, inputMessage);

		List<String> list = int2WrdResp.getErrorList();
		assertEquals(1, list.size());
		assertTrue(list.get(0).contains(UkError.INVALID_INPUT.getError()));

	}

	@Test
	public void testNumberIsNull() throws Exception {

		mockHttpRequest.setContent(jsonNullNumber.getBytes());
		handlerAdapter.handle(mockHttpRequest, mockHttpResponse, handler);

		assertEquals(STATUS_200_MSG, 200, mockHttpResponse.getStatus());

		String jsonResult = mockHttpResponse.getContentAsString();
		log.info("mockHttpResponse from IntToWordController: " + jsonResult);
		MockHttpInputMessage inputMessage = new MockHttpInputMessage(
				jsonResult.getBytes("UTF-8"));
		IntToWrdResp int2WrdResp = (IntToWrdResp) jack2HttpMsgCvtr
				.read(IntToWrdResp.class, inputMessage);
		assertEquals(0, int2WrdResp.getErrorList().size());

	}

	/**
	 * test null number ok as random int is generated.
	 * 
	 * @throws IOException
	 */
	@Test
	public void testNullNumberIsOk() throws Exception {

		mockHttpRequest.setContent(jsonMissingNumber.getBytes("UTF-8"));
		handlerAdapter.handle(mockHttpRequest, mockHttpResponse, handler);

		assertEquals(STATUS_200_MSG, 200, mockHttpResponse.getStatus());

		String jsonResult = mockHttpResponse.getContentAsString();
		log.info("mockHttpResponse from IntToWordController: " + jsonResult);
		MockHttpInputMessage inputMessage = new MockHttpInputMessage(
				jsonResult.getBytes("UTF-8"));
		IntToWrdResp int2WrdResp = (IntToWrdResp) jack2HttpMsgCvtr
				.read(IntToWrdResp.class, inputMessage);
		assertEquals(0, int2WrdResp.getErrorList().size());
	}

	/**
	 * 
	 * @throws IOException
	 */
	@Test
	public void testJsonReqIsValid() throws Exception {

		mockHttpRequest.setContent(this.jsonValidNumberAndProv.getBytes());

		handlerAdapter.handle(mockHttpRequest, mockHttpResponse, handler);

		assertEquals(STATUS_200_MSG, 200, mockHttpResponse.getStatus());

		String jsonResult = mockHttpResponse.getContentAsString();
		log.info("mockHttpResponse from IntToWordController: " + jsonResult);
		MockHttpInputMessage inputMessage = new MockHttpInputMessage(
				jsonResult.getBytes("UTF-8"));
		IntToWrdResp wordForIntegerResult = (IntToWrdResp) jack2HttpMsgCvtr
				.read(IntToWrdResp.class, inputMessage);
		assertEquals("one million", wordForIntegerResult.getWord());
	}

	/**
	 * Test method for
	 * {@link org.desz.mvc.controller.NumToWordRestController#appLandingPage()}
	 * .
	 * 
	 * @throws Exception
	 */
	@Test
	public void testAppLandingPage() throws Exception {

		String requestURI = IntToWordController.INT_TO_WORDAPP;
		Object getHandler;

		mockHttpRequest.setMethod("GET");
		mockHttpRequest.setContentType("application/text");
		mockHttpRequest.setRequestURI(requestURI);

		ModelAndView mav = new ModelAndView();

		getHandler = requestHandlerMapping.getHandler(mockHttpRequest)
				.getHandler();
		assertNotNull(getHandler);
		mav = handlerAdapter.handle(mockHttpRequest, mockHttpResponse,
				getHandler);
		assertEquals(STATUS_200_MSG, 200, mockHttpResponse.getStatus());

		assertNotNull(mav);
		assertEquals(
				String.format("view name should be %s",
						IntToWordController.VIEW_NAME),
				IntToWordController.VIEW_NAME, mav.getViewName());
		assertNotNull(mav.getModel());

	}

}
