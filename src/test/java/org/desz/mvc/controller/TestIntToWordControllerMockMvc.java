package org.desz.mvc.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.commons.lang3.StringUtils;
import org.desz.inttoword.language.ProvLang;
import org.desz.model.IntToWrdReq;
import org.desz.model.IntToWrdResp;
import org.desz.mvc.controller.IntToWordController;
import org.desz.mvc.controller.config.IntToWordControllerConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Loads the application context (MockMvc), ie not standalone
 * 
 * @author des
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ActiveProfiles({"dev", "cloud"})
@ContextConfiguration(classes = IntToWordControllerConfig.class)
public class TestIntToWordControllerMockMvc {

	@Autowired
	private WebApplicationContext wac;

	private MockMvc mockMvc;

	private String reqUkMill;

	private MockHttpServletRequestBuilder mockReqBuilder;

	private IntToWrdReq int2WrdReq;
	/**
	 * configure environment
	 * 
	 * @throws JsonProcessingException
	 */
	@Before
	public void setup() throws JsonProcessingException {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();

		mockReqBuilder = MockMvcRequestBuilders
				.post(IntToWordController.POSTREQ)
				.contentType(MediaType.APPLICATION_JSON);
		int2WrdReq = new IntToWrdReq();
		int2WrdReq.setNumber("1000000");
		int2WrdReq.setProvisionedLanguage(ProvLang.UK);
		this.reqUkMill = objectMapper.writeValueAsString(int2WrdReq);

	}

	@Test
	public void testEmptyLang() throws JsonProcessingException, Exception {
		final IntToWrdReq req = new IntToWrdReq();
		req.setProvisionedLanguage(ProvLang.EMPTY);
		// req.setValidEnum(ProvLang.EMPTY.getCode());
		mockMvc.perform(
				mockReqBuilder.content(objectMapper.writeValueAsString(req)))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.provisionedLanguage").value("EMPTY"));
	}

	/**
	 * Demo JSON in Http Request and Response
	 * 
	 * @throws Exception
	 */
	@Test
	public void demoExpectedJsonAndCharSet() throws Exception {
		mockMvc.perform(mockReqBuilder.content(reqUkMill))
				.andExpect(status().isOk()).andExpect(content()
						.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
	}

	// @Test
	public void invalidNumericInputJsonResponse() throws Exception {
		int2WrdReq.setNumber("-1000000");

		mockMvc.perform(mockReqBuilder
				.content(objectMapper.writeValueAsString(int2WordReq)))
				.andExpect(status().isOk())
				.andExpect(content().string(
						"{\"word\":null,\"number\":\"-1000000\",\"provisionedLanguage\":\"UK\","
								+ "\"errorList\":[\"Negative Integer disallowed\"],\"errors\":true}"));

	}

	@Test
	public void invalidNumericInputEmptyLang() throws Exception {
		int2WrdReq.setNumber("-1000000");
		int2WrdReq.setProvisionedLanguage(ProvLang.EMPTY);
		mockMvc.perform(mockReqBuilder
				.content(objectMapper.writeValueAsString(int2WrdReq)))
				.andExpect(status().isOk());
		// random sequence wrt language of error. TODO sort list.
				/*.andExpect(content().string(
						"{\"word\":null,\"number\":\"-1000000\",\"provisionedLanguage\":"
						+ "\"EMPTY\",\"errorList\":[\"Ongeldige Invoer/ungültige "
						+ "Eingabe/Invalid d'entrée/Invalid input\","
						+ "\"Selecteer een geldige taal/Wählen Sie eine gültige Sprache"
						+ "/Sélectionnez une langue valide/Select a valid language\"],"
						+ "\"errors\":true}"));
*/
	}

	@Test
	public void demoValidJsonResponse() throws Exception {
		mockMvc.perform(mockReqBuilder.content(reqUkMill))
				.andExpect(status().isOk())
				.andExpect(content().string(
						"{\"word\":\"one million\",\"number\":\"1000000\","
								+ "\"provisionedLanguage\":\"UK\",\"errorList\":[],\"errors\":false}"));

	}

	/**
	 * HTTP Response Code OK;EMPTY ProvLang detailed in response.
	 * 
	 * @throws Exception
	 */
	@Test
	public void demoInvalidRequestExpectStatusOk() throws Exception {
		mockMvc.perform(mockReqBuilder.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(int2WrdReq)))
				.andExpect(status().isOk());
	}

	// @Test
	public void emptyProvLangResponse() throws Exception {
		int2WrdReq.setProvisionedLanguage(ProvLang.EMPTY);

		mockMvc.perform(

				mockReqBuilder
						.content(objectMapper.writeValueAsString(int2WordReq)))
				.andExpect(status().isOk())
				.andExpect(content().string(expJsonRespEmptyLang));
	}

	/**
	 * POST_REQ should not handle HTTP GET
	 * 
	 * @throws Exception
	 */
	@Test
	public void httpGetMethodNotAllowed() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get(IntToWordController.POSTREQ)
				.contentType(MediaType.APPLICATION_JSON).content(reqUkMill))
				.andExpect(status().isMethodNotAllowed()).andReturn();
	}

	/**
	 * state : null number; random requested -> validates.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testRequestWithRandom() throws Exception {
		final IntToWrdReq req = new IntToWrdReq();
		req.setProvisionedLanguage(ProvLang.FR);
		MvcResult mvcRes = mockMvc
				.perform(mockReqBuilder
						.content(objectMapper.writeValueAsString(req)))
				.andReturn();
		assertNotNull("Response should not be null", mvcRes);
		final IntToWrdResp intToWordResp = objectMapper.readValue(
				mvcRes.getResponse().getContentAsString(), IntToWrdResp.class);
		assertFalse("should be false", intToWordResp.isErrors());
		assertEquals("should be 0", 0, intToWordResp.getErrorList().size());
		assertTrue("Word should not be empty",
				StringUtils.isNotEmpty(intToWordResp.getWord()));
	}

	private final String expJsonRespEmptyLang = "{\"word\":null,\"number\":1000000,\"provisionedLanguage\":\"EMPTY\",\"errorList\":[\"Select a valid language / Wählen Sie eine gültige Sprache / Selecteer een geldige taal / Sélectionnez une langue valide\"],\"errors\":true}";

	private final ObjectMapper objectMapper = new ObjectMapper();

	private IntToWrdReq int2WordReq;;
}