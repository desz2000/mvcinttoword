package org.desz.mvc.config;

import static org.junit.Assert.assertTrue;

import org.desz.inttoword.config.IntFreqRepoConfig;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ProfileTestNull {

	@Test
	public void test_abc() {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.getEnvironment()
				.setActiveProfiles(new String[]{"cloud", "dev"});
		// Enabled cloud profile
		context.register(IntFreqRepoConfig.class);
		context.refresh();
		assertTrue(context.containsBean("cloudrepo"));
		context.close();
	}

}
