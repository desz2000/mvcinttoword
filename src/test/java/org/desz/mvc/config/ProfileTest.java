package org.desz.mvc.config;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.desz.inttoword.config.IntFreqRepoConfig;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ProfileTest {

	@Test
	public void testManualLoad() {
		// load the context
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		// Enable dev profile.
		context.getEnvironment().setActiveProfiles("dev");
		context.register(IntFreqRepoConfig.class);
		context.refresh();
		assertFalse(context.containsBean("cloudrepo"));
		assertTrue(context.containsBean("dev_repo"));
		context.close();

	}

	@Test
	public void testShouldBeCloudRepo() {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.getEnvironment()
				.setActiveProfiles(new String[]{"cloud", "dev"});
		// Enable "cloud" profile
		context.register(IntFreqRepoConfig.class);
		context.refresh();
		assertTrue(context.containsBean("cloudrepo"));
		context.close();
	}

}
