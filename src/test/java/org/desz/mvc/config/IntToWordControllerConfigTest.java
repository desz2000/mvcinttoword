package org.desz.mvc.config;

import static org.junit.Assert.assertNotNull;

import org.desz.inttoword.service.INumberToWordService;
import org.desz.mvc.controller.config.IntToWordControllerConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

/**
 * Test IntToWordControllerConfig loaded
 * 
 * @author des
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration()
@ContextConfiguration(classes = IntToWordControllerConfig.class)
@ActiveProfiles({"dev", "cloud"})
public class IntToWordControllerConfigTest {

	@Autowired
	private INumberToWordService intToWordService;

	@Autowired
	private UrlBasedViewResolver viewResolver;

	@Test
	public void testConfig() {

		assertNotNull(viewResolver);
		assertNotNull(intToWordService);
	}

}
