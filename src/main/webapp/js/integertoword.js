/*
 * 
 * 
 * 
 * */

var json_req_mapping = "app_post.json";

/* Language variables */

var ln_map = [ {
	"pl" : "NL",
	"pltrans" : "Bevoorraad Taal:",
	"numtrans" : "Voer een geheel getal voor conversie:",
	"wordtrans" : "Woord:",
	"numres" : "Geheel getal:",
	"errs" : "Fouten:",
	"reqhdrsum" : "Aanvraag tot omzetting samenvatting",
	"buthlp" : "Acties",
	"rand" : "Genereert een willekeurig getal",
	"hlpwrd" : "Voer een geheel getal in en selecteer taal",
	"hlphdr" : "Gebruik:"

}, {
	"pl" : "DE",
	"pltrans" : "Provisioning Sprache:",
	"numtrans" : "Geben Sie eine Ganzzahl für die Konvertierung:",
	"wordtrans" : "Wort:",
	"numres" : "Ganze Zahl:",
	"errs" : "Fehler:",
	"reqhdrsum" : "Umtauschantrag Zusammenfassung",
	"buthlp" : "Aktionen",
	"rand" : "Erzeugt eine Zufallszahl",
	"hlpwrd" : "Geben Sie einen Integer und Sprache wählen",
	"hlphdr" : "Verwendung:"

}, {
	"pl" : "FR",
	"pltrans" : "Langue Provisionnés:",
	"numtrans" : "Entrer un entier pour le conversion:",
	"wordtrans" : "Mot:",
	"numres" : "Entier:",
	"errs" : "Erreur:",
	"reqhdrsum" : "Résumé demande de conversion",
	"buthlp" : "Actes",
	"rand" : "Génère un entier aléatoire",
	"hlpwrd" : "Entrez un nombre entier, puis sélectionnez la langue",
	"hlphdr" : "Usage:"

}, {
	"pl" : "UK",
	"pltrans" : "Provisioned Language:",
	"numtrans" : "Enter an integer for conversion:",
	"wordtrans" : "Word:",
	"numres" : "Integer:",
	"errs" : "Errors:",
	"reqhdrsum" : "Conversion Request Summary",
	"buthlp" : "Actions",
	"rand" : "Generates a random integer",
	"hlpwrd" : "Enter an integer and select language",
	"hlphdr" : "Usage:"
} ];
/* translate result labels */
function trans_labs(prov_ln) {

	$.each(ln_map, function(i, val) {
		if (val.pl === prov_ln) {
			$('#resnum_lab').html(val.numres);
			$('#word_lab').html(val.wordtrans);
			$('#errs_lab').html(val.errs);
			$('#reqhdrsum').html(val.reqhdrsum);
			$('#num_lab').html(val.numtrans);
			$('#provln_lab').html(val.pltrans);
			// $('#lang_lab').html(val.pltrans);
			$('#hlp_hdr').html(val.hlphdr).css({
				fontSize : "22px",
				color : "#317EAC"
			});

		}
	});

}

// anonymous function to translate innerHtml to selected language
$(function() {

	$("#provisionedLanguage").change(function(event) {
		$("option:selected", $(this)).each(function() {
			var obj = $('#provisionedLanguage').val();
			$.each(ln_map, function(i, val) {
				if (val.pl === obj) {
					$('#provln_lab').html(val.pltrans).css({
						fontSize : "22px",
						color : "#317EAC"
					});
					$('#num_lab').html(val.numtrans).css({
						fontSize : "22px",
						color : "#317EAC"
					});
					$('#p_rand').html(val.rand);
					$('#p_word').html(val.hlpwrd);
					$('#but_help').html(val.buthlp).css({
						fontSize : "22px",
						color : "#317EAC"
					});
					$('#hlp_hdr').html(val.hlphdr).css({
						fontSize : "22px",
						color : "#317EAC"
					});
				}
			});

		});
	});

});

/* refresh form */
function resetInputState() {

	$('#number').val(null);
	$('#provisionedLanguage').val("").attr('selected', true);
	trans_labs("UK");
	$("#audit").hide();
	$("#errors").hide();
	$("#reqhdr").collapse();

}

function selectText(divid) {
	if (document.selection) {
		var div = document.body.createTextRange();

		div.moveToElementText(document.getElementById(divid));
		div.select();
	} else {
		var div = document.createRange();
		div.setStartBefore(document.getElementById(divid));
		div.setEndAfter(document.getElementById(divid));

		window.getSelection().addRange(div);
	}

}

/*
 * post data asynchronously to Controller. b is truthy value that
 * may specify random integer generation by server
 */
function sendAjaxIntToWord(b) {

	var word = $("#word");
	var audit = $("#audit");
	var errors = $("#errors");
	var jList = $("#err_list");
	$(document).ready(function() {
		errors.collapse();
		audit.collapse();
		$("#resnum").text('');
		$("#reslang").text('');
		word.text('');
		$("#err_list").text('');
		// values to create JSON request
		var numberArg = $("#number").val();
		var provLangArg = $("#provisionedLanguage").val();
		// for display
		var selected = $("#provisionedLanguage option:selected").text();

		var jsonStr = null;
		if (b) {
			jsonStr = JSON.stringify({
				provisionedLanguage : provLangArg,
				number : null,
				randomRequested : true
			});

		} else {
			jsonStr = JSON.stringify({
				provisionedLanguage : provLangArg,
				number : numberArg
			});

			var obj = jQuery.parseJSON(jsonStr);
			// JSR-303 does not raise
			// violation for empty string hence convert to null
			if (obj.number === "") {
				obj.number = null;
				jsonStr = JSON.stringify(obj);
			}

		}
		console.log("json request:" + jsonStr);
		// json form: "{\"provisionedLanguage\":\"UK\",\"number\":1000000}" or
		// for random
		// {"provisionedLanguage":"FR","number":null,"randomRequested":true}

		var jqxhr = $.ajax({
			cache : false,
			type : "POST",
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json; charset=utf-8'
			},
			url : json_req_mapping,
			data : jsonStr
		});

		/* display the word or error value(s) from obj */
		jqxhr.done(function(msg) {
			resetInputState();
			var res = JSON.stringify(msg);

			var obj = jQuery.parseJSON(res);
			if (obj.word != null) {
				$("#resnum").append(obj.number);
				$("#reslang").append(selected);
				word.append(obj.word);
				// console.log("show audit");
				audit.show();
			} else {
				var errors = obj.errorList;
				audit.collapse();
				// console.log("collapsed audit");
				$("#errors").show();
				$.each(errors, function(index, item) {
					jList.append($("<li>" + item + "</li>"));
				});

			}
			$("#reqhdr").show();
			trans_labs(provLangArg);
		});

		jqxhr.fail(function(jqXHR, textStatus) {
			alert(jqXhrErrMsg(provLangArg) + textStatus);
		});

	});
}