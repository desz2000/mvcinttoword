<!DOCTYPE HTML>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!-- IntToWordReq Form bean -->
<jsp:directive.page import="org.desz.model.IntToWrdReq" />
<jsp:useBean id="intToWordReqBean" class="org.desz.model.IntToWrdReq"
	scope="request" />

<html>
<head>

<title>Convert Integer to Word</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<spring:url value="/info.html" var="info" />

<spring:url value="/bootstrap/css/bootstrap.min.cerulic.css"
	var="bootstrapMinCss" />

<spring:url value="/bootstrap/css/bootstrap-responsive.min.css"
	var="bootstrapResp" />

<link href="${bootstrapMinCss}" rel="stylesheet">
<link href="${bootstrapResp}" rel="stylesheet">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
	
<!-- 	<script
			  src="https://code.jquery.com/jquery-2.2.3.min.js"
			  integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo="
			  crossorigin="anonymous"></script> -->

<script src="js/integertoword.js" type="text/javascript" charset="utf-8"></script>

<spring:url value="/bootstrap/js/bootstrap-collapse2.2.1.js"
	var="bootStrapCollapseUrl" />
<script src="${bootStrapCollapseUrl}" type="text/javascript"></script>

<spring:url value="/bootstrap/js/bootstrap-dropdown2.0.js"
	var="bootStrapDropdownUrl" />
<script src="${bootStrapDropdownUrl}" type="text/javascript"></script>


</head>
<body>

	<div class="row-fluid">

		<div class="span4">
			<!--Left sidebar content-->
			<jsp:include page="../../info.html" />
		</div>

		<div class="span8" style="padding-top: 50px;">

			<jsp:include page="../../result.html" />

			<fieldset>
				<div class="control-group"
					style="padding-top: 30px; padding-left: 80px;">

					<div class="row-fluid">
						<div class="span5">
							<label id="num_lab" class="control-label"><font
								style="font-size: 22px; color: #317EAC">Enter an Integer
									:</font></label>
						</div>
						<div class="span1">
							<form:input name="number"
								style="padding-left: 0px; border-radius : 25px;border-width: 1px"
								path="intToWordReqBean.number" />

						</div>
					</div>


					<div class="row-fluid" id="language" style="padding-top: 30px;">
						<div class="span5">
							<label id="provln_lab" class="control-label"><font
								style="font-size: 22px; color: #317EAC">Provisioned
									Language:</font></label>
						</div>
						<div class="span1">
							<form:select name="provisionedLanguage"
								path="intToWordReqBean.provisionedLanguage"
								style="border-radius : 25px;border-width: 1px">
								<form:options items="${provisionedLanguages}" itemValue="code"
									itemLabel="description" />
							</form:select>

						</div>
					</div>
				</div>

				<div class="row-fluid"
					style="border-radius: 25px; border-width: 2px">
					<div class="span4" style="padding-top: 60px;">
						<div class="accordion" id="parent"
							style="padding-top: 40px; padding-left: 80px;">

							<div class="accordion-group">
								<label id="hlp_hdr"><font
									style="font-size: 22px; color: #317EAC">Usage:</font></label>

								<div class="accordion-heading"
									style="border-width: 1px; border-radius: 25px; padding-top: 0px; padding-left: 0px;">
									<a class="accordion-toggle" data-toggle="collapse"
										data-parent="parent" href="#usage"> <label id="but_help"><font
											style="font-size: 22px">Actions</font></label>
									</a>


									<div id="usage" class="accordion-body collapse"
										style="height: 0px;">

										<div class="accordion-inner">

											 <label><font
												style="font-size: 16px; color: #317EAC">Word:</font></label>
											<p id="p_word">
												<font style="font-size: 14px">Select language, an integer will be generated if not entered.</font>
											</p>

										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="span2" style="padding-top: 40px;">
						<div class="btn-group"
							style="padding-top: 60px; padding-left: 150px;">
							<button class="btn"
								style="color: #317EAC; border-width: 1.5px; border-radius: 25px; width: 110px; text-align: left">
								<font style="font-size: 22px">Actions</font>
							</button>
							<button class="btn btn-primary dropdown-toggle"
								data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li class="active" id="wrd_lnk"
									style="padding-left: 3px; padding-right: 3px"><a
									onclick='javascript:sendAjaxIntToWord()'><font
										style="font-size: 18px;">Get Word</font></a></li>
								<li class="inactive" style="padding-top: 4px"></li>
								<li class="active" style="padding-left: 3px; padding-right: 3px"><a
									onclick='javascript:resetInputState()'><font
										style="font-size: 18px">Reset</font></a></li>
							</ul>


						</div>
					</div>
				</div>
			</fieldset>
		</div>
	</div>
</body>
</html>