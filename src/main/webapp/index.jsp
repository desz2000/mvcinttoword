<!DOCTYPE html>

<html>
<head>
<title>Index</title>
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.cerulic.css"
	type="text/css" />
<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css"
	type="text/css" />

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="/bootstrap/js/bootstrap-dropdown2.0.js"
	type="text/javascript"></script>

</head>
<body>
	<div class="container" style="padding-top: 20px;">

		<div class="span9 offset2">
			<h3>Int Conversion Application:</h3>
		</div>
		<div class="row span9 offset2" style="padding-top: 20px;">
			<p style="padding-left: 5px; padding-top: 10px;">

				<font style="color: #317EAC; font-size: 16px;">This is a
					Spring / Java 8 application that converts an integer to
					representative word in chosen language. </font>

			</p>
		</div>

		<div class="row span9 offset2" style="padding-top: 20px;">

			<ul style="padding-top: 10px;">
				<li class="active"
					style="padding-left: 5px; border-width: 1.5px; border-radius: 25px;"><a
					href="numberToWord.htm"><font style="font-size: 20px">Int
							Conversion Application </font></a></li>
			</ul>

		</div>
		<div class="row span9 offset2">
			<p style="padding-top: 15px;">
				<font style="color: #317EAC; font-size: 16px">Converts
					positive integer [0 - 2,147,483,647] to language specific word
					(current languages: NL, DE, EN, FR).<br> The computational
					engine applies a recursive strategy to build the word.
				</font>
			</p>
			<p style="padding-top: 15px;">
				<font style="color: #317EAC; font-size: 16px">Input Integer
					and language are posted to and validated by Spring MVC controller.
					<br> The response will be either the word(s) or appropriate
					error message(s). <i>Nb., if not manually entered a randomly
						generated number will be sent to the computational engine</i>.
				</font>
			</p>

		</div>

	</div>


</body>
</html>