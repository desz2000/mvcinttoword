package org.desz.mvc.init;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.desz.mvc.controller.config.IntToWordControllerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class Initialiser
		extends
			AbstractAnnotationConfigDispatcherServletInitializer {
	final Logger log = LoggerFactory.getLogger(Initialiser.class);

	@Override
	public void onStartup(ServletContext servletContext)
			throws ServletException {
		super.onStartup(servletContext);
		servletContext.setInitParameter("spring.profiles.active", "dev, cloud");
	}

	@Override
	protected Class<?>[] getRootConfigClasses() {
		// not required due to import by 'servlet context'
		return null;
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[]{IntToWordControllerConfig.class};
	}

	// If the @Profile beans are loaded via servlet context

	@Override
	protected WebApplicationContext createServletApplicationContext() {

		WebApplicationContext context = super.createServletApplicationContext();
		((ConfigurableEnvironment) context.getEnvironment())
				.setActiveProfiles(new String[]{"dev", "cloud"});
		return context;

	}

	@Override
	protected String[] getServletMappings() {
		return new String[]{"*.htm", "*.json"};
	}
}
