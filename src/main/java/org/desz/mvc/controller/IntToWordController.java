package org.desz.mvc.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import org.desz.inttoword.exceptions.IntToWordServiceException;
import org.desz.inttoword.language.ProvLang;
import org.desz.inttoword.service.INumberToWordService;
import org.desz.model.IntToWrdReq;
import org.desz.model.IntToWrdResp;
import org.desz.model.validation.ErrorValueHelper;
import org.desz.random.generator.RandomGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

/**
 * Controller for application
 * 
 * @author des
 *
 */
@Component
@Controller
@SessionAttributes({"provisionedLanguages"})
public class IntToWordController {

	private final Logger log = LoggerFactory
			.getLogger(IntToWordController.class);

	@Autowired
	private INumberToWordService int2WrdService;

	public static final String POSTREQ = "/app_post.json";

	@Autowired
	private Validator validator;

	/**
	 * Method used by the Spring MVC framework
	 * 
	 * @return List<ProvLang>
	 */
	@ModelAttribute("provisionedLanguages")
	private List<ProvLang> getProvisionedLanguages() {
		return Arrays.asList(ProvLang.values());
	}

	/**
	 * 
	 * @param intToWrdReq
	 *            POJO with integer and language attributes
	 * @return JSON representation of IntToWrdResp instance in ResponseBody
	 */
	@RequestMapping(value = POSTREQ, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public final @ResponseBody IntToWrdResp getWordInJSON(
			@RequestBody final IntToWrdReq intToWrdReq) {
		IntToWrdResp intToWrdResp = new IntToWrdResp();

		final ProvLang provLang = intToWrdReq.getProvisionedLanguage();
		if (Objects.isNull(intToWrdReq.getNumber())) {
			// assign random number if null in int2WrdReq.
			final int randm = RandomGenerator.randomInt();
			log.info(String.format("Random integer assigned for conversion: %d",
					randm));
			intToWrdReq.setNumber(String.valueOf(randm));
		}
		// invoke JSR-305 validation.
		final Set<ConstraintViolation<IntToWrdReq>> violations = validator
				.validate(intToWrdReq);

		if (!violations.isEmpty())

		{
			BeanUtils.copyProperties(intToWrdReq, intToWrdResp);
			log.info(String.format("Violations for request state, %s",
					intToWrdReq.toString()));

			// add error message(s) to intToWrdResp according to target
			// language.
			intToWrdResp = ErrorValueHelper.setValidationError(intToWrdResp,
					violations);
			intToWrdResp.hasErrors(true);
			return intToWrdResp;
		}

		try

		{
			intToWrdResp.setWord(int2WrdService.getWordInLang(provLang,
					String.valueOf(intToWrdReq.getNumber())));
			BeanUtils.copyProperties(intToWrdReq, intToWrdResp);

		} catch (

		IntToWordServiceException e)

		{
			log.error(e.getMessage());
		}

		return intToWrdResp;

	}

	/**
	 * the HOME page
	 * 
	 * @see INT_TO_WORDAPP
	 * 
	 * @return uri for home page.
	 */
	@RequestMapping(value = INT_TO_WORDAPP, method = RequestMethod.GET)
	public String i2wHomeView(HttpServletRequest request) {
		return VIEW_NAME;
	}

	static final String INT_TO_WORDAPP = "/numberToWord.htm";
	static final String VIEW_NAME = "/applications/ajax/integertoword";

}
