package org.desz.mvc.controller.config;

import java.util.Objects;

import org.desz.inttoword.service.INumberToWordService;
import org.desz.inttoword.config.IntToWordServiceConfig;
import org.hibernate.validator.HibernateValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

@Configuration()
@Profile({"dev", "cloud"})
@EnableWebMvc
@ComponentScan(basePackages = {"org.desz.mvc.controller"})
@Import(value = {IntToWordServiceConfig.class})
public class IntToWordControllerConfig extends WebMvcConfigurerAdapter {
	final Logger log = LoggerFactory.getLogger(IntToWordControllerConfig.class);
	@Autowired
	private IntToWordServiceConfig intToWordConfig;

	/*
	 * @Bean(name = "checkCaseValidator") public ProvLangValidator
	 * checkCaseValidator() { return new ProvLangValidator(); }
	 */

	@Bean
	public INumberToWordService intToWordService() {
		Objects.requireNonNull(intToWordConfig);
		return intToWordConfig.intToWordService();
	}

	@Bean // (name = "viewResolver")
	public UrlBasedViewResolver getInternalResourceViewResolver() {
		UrlBasedViewResolver resolver = new UrlBasedViewResolver();
		resolver.setPrefix("/WEB-INF/jsp/");
		resolver.setSuffix(".jsp");
		resolver.setViewClass(JstlView.class);
		return resolver;
	}

	@Override
	public void configureDefaultServletHandling(
			DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

	@Bean
	public LocalValidatorFactoryBean validator() {
		LocalValidatorFactoryBean bean = new LocalValidatorFactoryBean();
		bean.setProviderClass(HibernateValidator.class);
		return bean;
	}

}
