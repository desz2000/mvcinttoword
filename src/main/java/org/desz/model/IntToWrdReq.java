package org.desz.model;

import java.io.Serializable;

import javax.annotation.Nullable;
import org.desz.inttoword.language.ProvLang;
import org.desz.model.validation.ProvLangConstraint;
import org.desz.model.validation.IntegerConstraint;

public final class IntToWrdReq implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	@ProvLangConstraint(value = ProvLang.EMPTY, message = "LANG_ERR")
	private ProvLang provisionedLanguage;

	/**
	 * message attribute is the name of XX_ERRORS (XX = NL, DE, UK, FR) Enum
	 * INVALID_INPUT
	 * 
	 * @see LanguageRepository
	 */
	@Nullable
	@IntegerConstraint(stringOfInt = String.class, message = "INVALID_INPUT")
	private String number;

	public String getNumber() {

		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	/**
	 * @return the provLn
	 */
	public ProvLang getProvisionedLanguage() {
		return provisionedLanguage;
	}

	/**
	 * @param provLn
	 *            the provLn to set
	 */
	public void setProvisionedLanguage(ProvLang provLn) {
		this.provisionedLanguage = provLn;
	}

	@Override
	public String toString() {
		return "IntToWrdReq [provLn=" + provisionedLanguage + ", number="
				+ number + "]";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((provisionedLanguage == null)
				? 0
				: provisionedLanguage.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IntToWrdReq other = (IntToWrdReq) obj;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (provisionedLanguage != other.provisionedLanguage)
			return false;
		return true;
	}

}
