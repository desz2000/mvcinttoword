package org.desz.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.desz.inttoword.language.ProvLang;

/**
 * 
 * @author des
 * 
 */
public final class IntToWrdResp implements Serializable {

	private static final long serialVersionUID = 1L;
	private String word;
	/* the request integer. */
	private String number;
	@NotNull
	private ProvLang provisionedLanguage;
	private List<String> errorList = new ArrayList<String>();
	private boolean errors;

	/**
	 * @return the provLn
	 */
	public ProvLang getProvisionedLanguage() {
		return provisionedLanguage;
	}

	/**
	 * @param provLn
	 *            the provLn to set
	 */
	public void setProvisionedLanguage(ProvLang provLn) {
		this.provisionedLanguage = provLn;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public final void setErrorList(List<String> errorList) {
		this.errorList = errorList;
	}

	public List<String> getErrorList() {
		return this.errorList;
	}

	public void addError(final String error) {
		errorList.add(error);
	}

	public void hasErrors(boolean errors) {

		this.errors = errors;
	}

	public boolean isErrors() {
		return this.errors;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"IntToWrdResp [word=%s, number=%s, provisionedLanguage=%s, errorList=%s, errors=%s]",
				word, number, provisionedLanguage, errorList, errors);
	}

}
