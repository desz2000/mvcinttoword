package org.desz.model.validation;

import java.util.Objects;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.desz.inttoword.language.ProvLang;

public class ProvLangValidator
		implements
			ConstraintValidator<ProvLangConstraint, ProvLang> {

	private ProvLangConstraint annotation;
	@Override
	public void initialize(ProvLangConstraint constraintAnnotation) {

		this.annotation = constraintAnnotation;
	}

	@Override
	public boolean isValid(ProvLang value, ConstraintValidatorContext context) {
		synchronized (annotation) {
			if (Objects.isNull(value))
				return false;
			return !value.toString().equalsIgnoreCase(ProvLang.EMPTY.getCode());
		}
	}

}
