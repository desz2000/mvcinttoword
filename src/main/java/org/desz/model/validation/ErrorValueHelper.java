/**
 * sets language specific error messages
 */
package org.desz.model.validation;

import java.util.Objects;
import java.util.Set;

import javax.validation.ConstraintViolation;

import org.desz.inttoword.language.*;
import org.desz.inttoword.language.ProvLangFactoryParts.DeError;
import org.desz.inttoword.language.ProvLangFactoryParts.FrError;
import org.desz.inttoword.language.ProvLangFactoryParts.NlError;
import org.desz.inttoword.language.ProvLangFactoryParts.UkError;
import org.desz.model.IntToWrdReq;
import org.desz.model.IntToWrdResp;

/**
 * @author des
 * 
 */
public class ErrorValueHelper {

	private ErrorValueHelper() {
	}

	/**
	 * 
	 * @param int2WrdResp
	 *            the IntToWrdResp.
	 * @param violations
	 *            the ConstraintViolations.
	 * @return int2WrdResp the IntToWrdResp.
	 */
	public static final IntToWrdResp setValidationError(
			IntToWrdResp int2WrdResp,
			Set<ConstraintViolation<IntToWrdReq>> violations) {
		int2WrdResp = Objects.requireNonNull(int2WrdResp,
				"service requires non-null parameter");
		final ProvLang provLn = int2WrdResp.getProvisionedLanguage();
		// add language specific error(s).
		for (ConstraintViolation<IntToWrdReq> violn : violations)
			int2WrdResp = assignError(violn.getMessageTemplate(), provLn,
					int2WrdResp);

		return int2WrdResp;

	}

	/**
	 * Add chosen language error or each translation if ProvLang.EMPTY
	 * 
	 * @param enumName
	 * @param provLang
	 * @param int2WrdResp
	 * @return int2WrdResp the Int2WrdResp.
	 */
	private static IntToWrdResp assignError(final String enumName,
			final ProvLang provLang, IntToWrdResp int2WrdResp) {

		StringBuilder err = new StringBuilder();
		switch (provLang) {
			case UK :
				for (UkError uk : UkError.values()) {
					if (uk.name().equals(enumName))
						err.append(uk.getError());

				}
				break;
			case FR :

				for (FrError fr : FrError.values()) {
					if (fr.name().equals(enumName))
						err.append(fr.getError());

				}
				break;
			case DE :

				for (DeError de : DeError.values()) {
					if (de.name().equals(enumName))
						err.append(de.getError());

				}
				break;
			case NL :

				for (NlError nl : NlError.values()) {
					if (nl.name().equals(enumName))
						err.append(nl.getError());

				}
				break;
			default :
				for (NlError nl : NlError.values()) {
					if (nl.name().equals(enumName))
						err.append(nl.getError() + "/");

				}
				for (DeError de : DeError.values()) {
					if (de.name().equals(enumName))
						err.append(de.getError() + "/");

				}
				for (FrError fr : FrError.values()) {
					if (fr.name().equals(enumName))
						err.append(fr.getError() + "/");

				}
				for (UkError uk : UkError.values()) {
					if (uk.name().equals(enumName))
						err.append(uk.getError());

				}

				break;

		}
		int2WrdResp.addError(err.toString());
		return int2WrdResp;
	}

}
