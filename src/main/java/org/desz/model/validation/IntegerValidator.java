package org.desz.model.validation;
import java.util.Objects;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class IntegerValidator
		implements
			ConstraintValidator<IntegerConstraint, String> {

	private IntegerConstraint annotation;
	@Override
	public void initialize(IntegerConstraint constraintAnnotation) {
		this.annotation = constraintAnnotation;
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		synchronized (annotation) {
			if (Objects.isNull(value))
				return true;
			if (!value.matches("\\d+"))
				return false;
			if (value.length() > 10)
				return false;
			if (value.length() == 10) {

				if (Character.getNumericValue(
						value.charAt(value.length() - 1)) > 7) {
					return false;
				}

			}

			return Integer.valueOf(value) > 0;

		}
	}

}
