/**
 * sets language specific error messages
 */
/*
 * package org.desz.model.validation;
 * 
 * import java.util.Arrays; import java.util.Collections; import
 * java.util.HashMap; import java.util.List; import java.util.Map; import
 * java.util.Set; import java.util.stream.Collectors;
 * 
 * import javax.validation.ConstraintViolation;
 * 
 * import org.apache.commons.lang3.StringUtils; import
 * org.desz.inttoword.language.ILangProvider; import
 * org.desz.inttoword.language.LanguageRepository.ProvLang; import
 * org.desz.inttoword.language.LanguageRepository.UkError; import
 * org.desz.inttoword.language.ProvLangFactory; import
 * org.desz.model.IntToWrdReq; import org.desz.model.IntToWrdResp;
 * 
 *//**
	 * @author des
	 * 
	 */
/*
 * public class ErrorTranslater {
 * 
 * private static Map<ProvLang, ILangProvider> PROV_LANG_CACHE = Collections
 * .synchronizedMap(new HashMap<ProvLang, ILangProvider>());
 * 
 *//**
	 * 
	 * @param ln
	 *            the ProvLang.
	 */
/*
 * private static void addProvLangFactoryToCache(ProvLang ln) {
 * 
 * if (!(PROV_LANG_CACHE.containsKey(ln))) PROV_LANG_CACHE.put(ln,
 * ProvLangFactory.getInstance().factoryForProvLang(ln));
 * 
 * }
 * 
 *//**
	 * 
	 * @param p
	 * @return ILangProvider for p.
	 */
/*
 * private static ILangProvider getProvLangFactoryFromCache(ProvLang p) { return
 * PROV_LANG_CACHE.get(p); }
 * 
 *//**
	 * 
	 * @param int2WrdResp.
	 * @param violations.
	 * @return language specific error messages.
	 *//*
	 * public static final IntToWrdResp setValidationError( final IntToWrdResp
	 * int2WrdResp, Set<ConstraintViolation<IntToWrdReq>> violations) {
	 * 
	 * List<String> errorList = violations.stream()
	 * .map(ConstraintViolation::getMessageTemplate)
	 * .collect(Collectors.toList());
	 * 
	 * // remove invalid lang. message from errorList List<String> filterErrKeys
	 * = errorList.stream() .filter(s -> (!s.contains("Invalid")))
	 * .collect(Collectors.toList());
	 * 
	 * final ProvLang provLang = int2WrdResp.getProvisionedLanguage(); final
	 * boolean validProvLang = provLang.isValid();
	 * 
	 * List<ProvLang> validProvLangs = Arrays.asList(ProvLang.values())
	 * .stream().filter(p -> (!p.equals(ProvLang.EMPTY)))
	 * .collect(Collectors.toList());
	 * 
	 * for (ProvLang p : validProvLangs) addProvLangFactoryToCache(p); // Check
	 * if pl.isValid because JSR303 does not detect. if (!validProvLang) {
	 * StringBuilder sb = new StringBuilder(); if (!filterErrKeys.isEmpty()) {
	 * for (ProvLang p : validProvLangs) {
	 * 
	 * String s = getProvLangFactoryFromCache(p) .getErrorForProvLang(p,
	 * filterErrKeys.get(0)); sb.append(s + StringUtils.LF + "/"); }
	 * int2WrdResp.addError( StringUtils.substringBefore(sb.toString(), "/"));
	 * sb.setLength(0); } // add select language message for each valid
	 * language. for (ProvLang p : validProvLangs) { String s =
	 * getProvLangFactoryFromCache(p).getErrorForProvLang(p,
	 * UkError.LANG_ERR.name()); sb.append(s + StringUtils.LF + "/"); }
	 * 
	 * int2WrdResp .addError(StringUtils.substringBefore(sb.toString(), "/")); }
	 * // add valid ProvLang errors. for (String err : filterErrKeys) { if
	 * (validProvLang) { String s = getProvLangFactoryFromCache(provLang)
	 * .getErrorForProvLang(provLang, err); int2WrdResp.addError(s +
	 * StringUtils.LF); } }
	 * 
	 * return int2WrdResp;
	 * 
	 * }
	 * 
	 * }
	 */