package org.desz.model.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ElementType.FIELD, ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Constraint(validatedBy = {IntegerValidator.class})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface IntegerConstraint {

	public abstract String message() default "Invalid numeric value.";

	public abstract Class<?>[] groups() default {};

	public abstract Class<? extends Payload>[] payload() default {};

	// public abstract Class<? extends java.lang.String> stringOfInt();

	@Target({ElementType.FIELD, ElementType.METHOD,
			ElementType.ANNOTATION_TYPE})
	@Retention(RetentionPolicy.RUNTIME)
	@Documented
	@interface List {
		IntegerConstraint[] value();
	}

	public abstract Class<String> stringOfInt();
}
