package org.desz.model.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.desz.inttoword.language.ProvLang;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER,
		ElementType.ANNOTATION_TYPE})
@Constraint(validatedBy = {ProvLangValidator.class})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ProvLangConstraint {
	public abstract String message() default "Invalid provisioned lang value.";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	// public abstract Class<? extends java.lang.Enum<?>> enumClass();

	ProvLang value();

	@Target({ElementType.FIELD, ElementType.METHOD,
			ElementType.ANNOTATION_TYPE})
	@Retention(RetentionPolicy.RUNTIME)
	@Documented
	@interface List {
		ProvLangConstraint[] value();
	}
}
